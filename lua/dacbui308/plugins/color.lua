return {
	"Mofiqul/dracula.nvim",
	config = function()
		require("dracula").setup({
			transparent_bg = false,
		})
		Transarent("dracula")
	end,
}
